function invertFunction(obj) {
  if (typeof obj !== "object" || obj === null) {
    throw new Error("Input data is not correct");
  }
  let entries = [];
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      entries.push([obj[key], key]);
    }
  }
  let result = {};
  for (let [value, key] of entries) {
    result[value] = key;
  }
  return result;
}

module.exports = invertFunction;
