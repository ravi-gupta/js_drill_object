function defaultsFunction(obj, defaultProps) {
    if (
        typeof obj !== "object" ||
        obj === null ||
        typeof defaultProps !== "object" ||
        defaultProps === null
    ) {
        throw new Error("Invalid input: inputs should be objects");
    }
    for (let key in defaultProps) {
        if (defaultProps.hasOwnProperty(key) && obj[key] === undefined) {
            obj[key] = defaultProps[key];
        }
    }
    return obj;
}

module.exports = defaultsFunction;
