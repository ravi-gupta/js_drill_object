function mapObject(obj, callBack) {
    if (
        typeof obj !== "object" ||
        obj === null ||
        typeof callBack !== "function"
    ) {
        throw new Error("Input data is not correct");
    }
    let result = {};
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            result[key] = callBack(obj[key]);
        }
    }
    return result;
}
module.exports = mapObject;
