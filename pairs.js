function pairFunction(obj) {
  if (typeof obj !== "object" || obj === null) {
    throw new Error("Input data is not correct");
  }
  let result = [];
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      let subArray = [];
      subArray.push(key);
      subArray.push(obj[key]);
      result.push(subArray);
    }
  }
  return result;
}
module.exports = pairFunction;
