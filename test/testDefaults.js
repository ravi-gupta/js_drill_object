const defaultFunction = require("../defaults");
const data = { name: "Bruce Wayne", age: 36 };

const defaultProps = { age: 30, location: "Gotham" };

const arrFromDefaultFunction = defaultFunction(data, defaultProps);
console.log(arrFromDefaultFunction);
