function keysFunctionByMe(obj) {
  if (typeof obj !== "object" || obj === null) {
    throw new Error("Input data is not correct");
  }
  let result = [];
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      result.push(key);
    }
  }
  return result;
}
module.exports = keysFunctionByMe
